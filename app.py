import datetime
import random

from flask import (flash, Flask, redirect, render_template, request,
                   session, url_for, send_file, jsonify)

import concurrent.futures
from werkzeug.utils import secure_filename
import os, uuid
import time
import json
import csv
import requests, logging
from random import choice
import asyncio
import aiohttp

app = Flask(__name__)
app.config["output_li"] = []
app.config["event_id"] = ""


UPLOAD_FOLDER = 'static/uploads/'
ALLOWED_EXTENSIONS = set(['csv', 'xlsx'])

# SqlAlchemy Database Configuration With Mysql
app.config["SECRET_KEY"] = "sdfsf65416534sdfsdf4653"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
secure_type = "http"

server_file_name = "server.log"
logging.basicConfig(filename=server_file_name, level=logging.DEBUG)
secreatid = uuid.uuid4().hex
app.logger.debug(f"secreadid is {secreatid}")

def delete_text_file(file_path):
    with open(file_path, 'w') as file:
        file.truncate(0)
    return "complate"

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def generate_UA(firefox_user_agents):return choice(firefox_user_agents)
def generate_reese84(reese84):return choice(reese84)

def get_headers(api_key):
    try:
        url = "https://headers-gold.vercel.app/headers/"
        payload = {}
        headers = {
            'accept': 'application/json',
            'api-key': api_key
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        all_response_data = json.loads(response.text)
        return all_response_data

    except Exception as e:
        print(e)

async def get_api_response_for_event_id(event_id, session, firefox_user_agents, reese84, get_heeader_list):
    headers = random.choice(get_heeader_list)
    base_url = "https://offeradapter.ticketmaster.com/api/ismds/event/{event_id}/facets?show=listpricerange&by=offers%20inventoryTypes%20accessibility%20section&apikey=b462oi7fic6pehcdkzony5bxhe&apisecret=pquzpfrfz7zd2ylvtz3w5dtyse"
    current_url = base_url.format(event_id=event_id)
    n = 0
    while True:
        try:
            async with session.get(current_url, headers=headers, proxy="http://production_589243:R5C3KxvZ6NyU_country-us@geo.iproyal.com:12321") as response:
                if response.status == 200:
                    data = await response.json()
                    return event_id, data
                elif response.status == 403 and n < 5:
                    print(f"Encountered 403 error for Event ID {event_id}. Retrying with a different token.")
                    n+=1
                    continue  # Retry with a different token
                else:
                    print(f"Failed to retrieve data for Event ID {event_id}. Status code: {response.status}")
                    # break
                    return None, None
        except Exception as e:
            print(f"Failed to retrieve data for Event ID {event_id}. Error: {e}")
            return None, None

async def process_events(event_ids, final_data, firefox_user_agents, reese84, get_heeader_list):
    async with aiohttp.ClientSession() as session:
        tasks = [get_api_response_for_event_id(event_id, session, firefox_user_agents, reese84, get_heeader_list) for event_id in event_ids]
        results = await asyncio.gather(*tasks)
        for event_id, data in results:
            if data is not None:
                final_data[event_id] = data
                print(f"Event ID: {event_id}")
                print(json.dumps(data, indent=4))  # Pretty print JSON data
   
@app.route("/tickermaster", methods=["GET", "POST"])
def tickermaster():
    try:
        output_file_path = os.path.abspath("log_main.txt")
        current_date_time = datetime.datetime.now()
        with open(output_file_path, "a") as file:
            file.write(str(current_date_time) + ":" + "  coming in ticketmaster api route  :  Success\n")
        start_time = time.time()
        input_data = request.files['file']
        path = app.config["UPLOAD_FOLDER"]
        final_data = {}
        delete_text_file("output.json")

        if 'file' not in request.files:
            flash('No file part')
            redirect(url_for('home_file', _external=True, _scheme=secure_type))

        if input_data.filename == '':
            flash('No resume selected for uploading')
            redirect(url_for('home_file', _external=True, _scheme=secure_type))

        exten = ""
        if input_data and allowed_file(input_data.filename):
            filename = secure_filename(input_data.filename)
            exten = filename.split(".")[-1]
            input_data.save(os.path.join(path, filename))
            csv_file_path = os.path.join(path, filename)
        else:
            flash('This file format is not supported.....')
            return redirect(url_for('home', _external=True, _scheme=secure_type))

        firefox_user_agents = []
        reese84 = []

        t = time.time()
        final_data = {}
             
        with open(csv_file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            event_ids = [row['event_id'] for row in reader]

        # Run the event processing asynchronously
        get_heeader_list = get_headers(f"kW3hLx9Esdbhjbf{secreatid}")
        asyncio.run(process_events(event_ids,final_data, firefox_user_agents, reese84, get_heeader_list))
        json_file_path = "output.json"

        # Write data to JSON file
        with open(json_file_path, 'w') as json_file:
            json.dump(final_data, json_file, indent=4)

        message = f"Process Completed. Please download your output file using Download button\nTime taken for that process {time.time()-start_time}"

        return json.dumps({"message":message, "json_data":final_data})

    except Exception as e:
        message = "Server Not Responding"
        app.logger.debug(f"Error in main route: {e}")
        return jsonify(message=message)


@app.route("/tickermaster_ui", methods=["GET", "POST"])
def tickermaster_ui():
    try:
        output_file_path = os.path.abspath("log_main.txt")
        current_date_time = datetime.datetime.now()
        with open(output_file_path, "a") as file:
            file.write(str(current_date_time) + ":" + "  coming in ticketmaster UI route  :  Success\n")
        start_time = time.time()
        input_data = request.files['file']
        path = app.config["UPLOAD_FOLDER"]
        final_data = {}
        delete_text_file("output.json")

        if 'file' not in request.files:
            flash('No file part')
            return jsonify(message='No resume selected for uploading')

        if input_data.filename == '':
            flash('No resume selected for uploading')
            return jsonify(message='No resume selected for uploading')

        exten = ""
        if input_data and allowed_file(input_data.filename):
            filename = secure_filename(input_data.filename)
            exten = filename.split(".")[-1]
            input_data.save(os.path.join(path, filename))
            csv_file_path = os.path.join(path, filename)
        else:
            flash('This file format is not supported.....')
            return jsonify(message='No resume selected for uploading')

        firefox_user_agents = []
        reese84 = []

        t = time.time()
        final_data = {}

        with open(csv_file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            event_ids = [row['event_id'] for row in reader]

        # Run the event processing asynchronously
        get_heeader_list = get_headers(f"kW3hLx9Esdbhjbf{secreatid}")
        asyncio.run(process_events(event_ids, final_data, firefox_user_agents, reese84, get_heeader_list))
        json_file_path = "output.json"

        # Write data to JSON file
        with open(json_file_path, 'w') as json_file:
            json.dump(final_data, json_file, indent=4)

        message = f"Process Completed. Please download your output file using Download button\nTime taken for that process {time.time() - start_time}"

        return jsonify(message=message)

    except Exception as e:
        message = "Server Not Responding"
        app.logger.debug(f"Error in main route: {e}")
        return jsonify(message=message)

@app.route("/clean_server_logs", methods=["GET", "POST"])
def clean_server_logs():
    try:
        file_path = os.path.abspath("server.log")
        # Delete the file if it exists
        if os.path.exists(file_path):
            os.remove(file_path)

        # Recreate the file
        with open(file_path, 'w'):
            pass

        return redirect(url_for('view_server_logs', _external=True, _scheme=secure_type))

    except Exception as e:
        message = "Server Not Responding"
        app.logger.debug(f"Error in main route: {e}")
        return redirect(url_for('view_server_logs', _external=True, _scheme=secure_type))

@app.route("/clean_logs", methods=["GET", "POST"])
def clean_logs():
    try:
        file_path = os.path.abspath("log_main.txt")
        # Delete the file if it exists
        if os.path.exists(file_path):
            os.remove(file_path)

        # Recreate the file
        with open(file_path, 'w'):
            pass

        return redirect(url_for('view_logs', _external=True, _scheme=secure_type))

    except Exception as e:
        message = "Server Not Responding"
        app.logger.debug(f"Error in main route: {e}")
        return redirect(url_for('view_logs', _external=True, _scheme=secure_type))

@app.route("/", methods=["GET", "POST"])
def home():
    try:
        output_file_path = os.path.abspath("log_main.txt")
        current_date_time = datetime.datetime.now()
        with open(output_file_path, "a") as file:
            file.write(str(current_date_time) + ":" + "  coming in ticketmaster home route  :  Success\n")
        return render_template("index.html")

    except Exception as e:
        return render_template("index.html")

@app.route("/download_server_logs", methods=['GET'])
def download_server_logs():
    file = os.path.abspath("output.json")
    return send_file(file, as_attachment=True)

@app.route("/view_logs", methods=['GET'])
def view_logs():
    try:
        file = os.path.abspath("log_main.txt")
        lines = []
        with open(file, "r") as f:
            lines += f.readlines()
        return render_template("logs.html", lines=lines)

    except Exception as e:
        print(e)

@app.route("/download_logs", methods=['GET'])
def download_logs():
    try:
        server_file_name = "log_main.txt"
        file = os.path.abspath(server_file_name)
        return send_file(file, as_attachment=True)

    except Exception as e:
        print(e)

@app.route("/view_server_logs", methods=['GET'])
def view_server_logs():
    try:
        server_file_name = "server.log"
        file = os.path.abspath(server_file_name)
        lines = []
        with open(file, "r") as f:
            lines += f.readlines()
        return render_template("logs_server.html", lines=lines)

    except Exception as e:
        app.logger.debug(f"Error in show log api route : {e}")
        return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
